# DTPlugin
> 为保护知识产权，本系统已申请软著。

#### 介绍
leaflet库的微信小程序插件版

leafletwx（leaflet小程序版）地址：[leafletwx](https://gitee.com/zz2022com/leafletwx)

后台管理系统地址：[ScenicAreaMapAdmin](https://gitee.com/zz2022com/ScenicAreaMapAdmin)

卓伙微信小程序的地图插件，可以轻松实现加载自定义的地图瓦片。
本插件在leaflet的基础上开发，算是leaflet的微信小程序版本。
目前默认加载QQ地图瓦片数据，通过本插件通过简单的修改，可以加载诸如百度、高德等三方的瓦片地图数据

![输入图片说明](doc/media/a9c8bb0fd0d07a6b93d3f4b1244e29a7.png)

使用自定义瓦片地图示例：


![输入图片说明](doc/media/zdy.jpg)

使用默认高德瓦片地图示例：


![输入图片说明](doc/media/mr.jpg)


#### 软件架构
前端为微信小程序，后端使用geodjango实现。
目前后端使用Ubuntu 20.04，Pyhton版本为3.8.10

#### 插件集成方法
参见本项目wiki


#### 使用说明
参见[后端项目wiki](https://gitee.com/zz2022com/ScenicAreaMapAdmin/wiki)

### 联系我
微信：zz2022su
