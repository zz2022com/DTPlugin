# 卓伙自定义瓦片地图

本系统由前端小程序和后台管理组成。前端为小程序可以为用户提供完全自定义的多级瓦片地图显示功能，并完全支持腾讯地图的GCJ-02坐标系；后台管理系统提供地图瓦片、POI等编辑功能，并为小程序提供接口服务。
本系统可以为景区、度假区、展览馆、医院等场所提供导览功能，包含自定义个性化地图、景点讲解、游览路线、演出时间提醒、定位等功能。

## 小程序

### 地图展示
![链接](./map_page.png)

### POI详细信息
![链接](./poi_detail.png)

### 导览路线
![链接](./road.png)

### 演出时刻表
![链接](./show_time.png)

### 插件集成方法
目标小程序类目需有服务类目：“旅游服务 > 景区服务”
可在小程序“设置”中的“基本设置”标签，修改添加“服务类目”。

"app.json"中添加插件
```json
"plugins": {
    "zhgeo-plugin": {
      "version": "x.x.x",   // 插件版本
      "provider": "wx10256c25ad55ad98"
    }
  },
  "useExtendedLib": {
    "weui": true
  },
  "requiredPrivateInfos" : [
    "getLocation"
  ],
  "permission": {
    "scope.userLocation": {
      "desc": "你的位置信息将用于小程序位置接口的效果展示"
    }
  }
```

以在index页面中添加地图插件为例
index.js
```javascript
const plugin = requirePlugin('zhgeo-plugin')
// plugin.setServeUrl('http://192.168.2.18:8000')
Page({
  data: {
    rootScenicId: 0
  },
  onLoad(options) {
    if (options.scene) {
        this.setData({
          rootScenicId: parseInt(options.scene)
        })
    } else {
        this.setData({
          rootScenicId: options.rootScenicId ?? 0
        })
    }
  },
  onUnload: function () {
    plugin.onUnloadPage();
  },

  changeScenic: function(e) {
    this.pageRouter.navigateTo({
      url: 'index?rootScenicId=' + e.detail.scenicId
    })
  },

  redirectScenic: function(e) {
    this.pageRouter.redirectTo({
      url: 'index?rootScenicId=' + e.detail.scenicId
    })
  },

  updateBarTitle: function(e) {
    wx.setNavigationBarTitle({
        title: e.detail.scenicName
    })
  }
})
```

index.json
```json
{
  "usingComponents": {    
    "v-zhgeo": "plugin://zhgeo-plugin/zhgeo-component"
  }
}
```

index.wxml
```html
<view class="container">
  <v-zhgeo rootScenicId="{{rootScenicId}}" bind:changeScenic="changeScenic" bind:redirectScenic="redirectScenic" bind:updateBarTitle="updateBarTitle" />
</view>
```

## 后台管理系统
![链接](./bgmng.png)
